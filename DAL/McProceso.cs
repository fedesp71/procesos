﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class McProceso
    {
        Acceso acceso = new Acceso();

        public void Alta(ProcesoEntity proceso)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nombre", proceso.Nombre));
            parameters.Add(acceso.CrearParametro("@TiempoEjec", proceso.TiempoEjec));
            acceso.Escribir("spAltaProcesos", parameters);

            acceso.Cerrar();
        }

        public List<ProcesoEntity> Cargar()
        {
            List<ProcesoEntity> procesos = new List<ProcesoEntity>();

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spCargarProcesos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ProcesoEntity proceso = new ProcesoEntity();
                proceso.Nombre = registro["Nombre"].ToString();
                proceso.TiempoEjec = registro["TiempoEjec"].ToString();

                procesos.Add(proceso);
            }
            return procesos;
        }
    }
}
