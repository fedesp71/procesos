﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ProcesoBusiness
    {
        McProceso mc = new McProceso();
        public void Alta(List<ProcesoEntity> procesos)
        {
            foreach(ProcesoEntity proceso in procesos)
            {
                mc.Alta(proceso);
            }
        }
        public List<ProcesoEntity> Cargar()
        {
           return mc.Cargar();
        }
    }
}
