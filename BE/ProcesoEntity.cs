﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ProcesoEntity
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string tiempoEjec;

        public string TiempoEjec
        {
            get { return tiempoEjec; }
            set { tiempoEjec = value; }
        }

    }
}
