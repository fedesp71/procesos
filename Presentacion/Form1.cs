﻿using BE;
using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ProcesoBusiness gestor = new ProcesoBusiness();

        private void Form1_Load(object sender, EventArgs e)
        {
            // Process p = (Process)lstProcesos.SelectedItem;
            ListarProcesos();
        }

        public void ListarProcesos()
        {
            lstProcesos.DataSource = null;
            lstProcesos.DataSource = Process.GetProcesses();
            lstProcesos.DisplayMember = "ProcessName";
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtParametros.Text))
            {
                Process.Start(txtNombre.Text);
            }
            else
            {
                Process.Start(txtNombre.Text, txtParametros.Text);
            }

            ListarProcesos();
        }

        public List<Process> ListaProcesos()
        {
            List<Process> procesos = new List<Process>();
            foreach (var listBoxItem in lstProcesos.Items)
            {
                procesos.Add((Process)listBoxItem);
            }
            return procesos;
        }

        public string TiempoEjec(Process proceso)
        {
            string p = "";
            try
            {
              p =  proceso.TotalProcessorTime.ToString();
            }
            catch 
            {
                p = "Sin permiso";
            }
            return p;
        }

        private void btnXml_Click(object sender, EventArgs e)
        {
            CrearXml();
        }

        public XmlDocument CrearXml()
        {
            XmlDocument xml = new XmlDocument();
            XmlElement root = xml.CreateElement("Procesos");
            xml.AppendChild(root);
            foreach (Process proceso in ListaProcesos())
            {
                XmlElement child = xml.CreateElement("proceso");
                child.SetAttribute("Nombre", proceso.ProcessName);
                child.SetAttribute("TiempoEjecucion", TiempoEjec(proceso));
                root.AppendChild(child);
            }

            return xml;
        }

        private void btlListarXml_Click(object sender, EventArgs e)
        {
            ListarXml(CrearXml());
        }

        public void ListarXml(XmlDocument xml)
        {
            dtProcesos.DataSource = null;
            DataSet ds = new DataSet();
            XmlNodeReader xnr = new XmlNodeReader(xml);
            ds.ReadXml(xnr);
            dtProcesos.DataSource = ds.Tables[0];
        }

        private void btnGuardarSql_Click(object sender, EventArgs e)
        {
            List<ProcesoEntity> procesosEntity = new List<ProcesoEntity>();

            foreach (Process proceso in ListaProcesos())
            {
                ProcesoEntity procesoEntity = new ProcesoEntity();
                procesoEntity.Nombre = proceso.ProcessName;
                procesoEntity.TiempoEjec = TiempoEjec(proceso);
                procesosEntity.Add(procesoEntity);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardaSql_Click(object sender, EventArgs e)
        {
            List<ProcesoEntity> procesosEntity = new List<ProcesoEntity>();
            foreach (Process proceso in ListaProcesos())
            {
                ProcesoEntity procesoEntity = new ProcesoEntity();
                procesoEntity.Nombre = proceso.ProcessName;
                procesoEntity.TiempoEjec = TiempoEjec(proceso);
                procesosEntity.Add(procesoEntity);
            }
            gestor.Alta(procesosEntity);
        }

        private void btnListarSql_Click(object sender, EventArgs e)
        {
            dtProcesos.DataSource = null;
            dtProcesos.DataSource = gestor.Cargar();
        }
    }
}
