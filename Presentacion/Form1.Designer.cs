﻿namespace Presentacion
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstProcesos = new System.Windows.Forms.ListBox();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtParametros = new System.Windows.Forms.TextBox();
            this.btnXml = new System.Windows.Forms.Button();
            this.dtProcesos = new System.Windows.Forms.DataGridView();
            this.btlListarXml = new System.Windows.Forms.Button();
            this.btnListarSql = new System.Windows.Forms.Button();
            this.btnGuardarSql = new System.Windows.Forms.Button();
            this.btnGuardaSql = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtProcesos)).BeginInit();
            this.SuspendLayout();
            // 
            // lstProcesos
            // 
            this.lstProcesos.FormattingEnabled = true;
            this.lstProcesos.ItemHeight = 16;
            this.lstProcesos.Location = new System.Drawing.Point(12, 12);
            this.lstProcesos.Name = "lstProcesos";
            this.lstProcesos.Size = new System.Drawing.Size(120, 292);
            this.lstProcesos.TabIndex = 0;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(326, 62);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(112, 44);
            this.btnEjecutar.TabIndex = 1;
            this.btnEjecutar.Text = "Abir";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(148, 61);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 22);
            this.txtNombre.TabIndex = 2;
            // 
            // txtParametros
            // 
            this.txtParametros.Location = new System.Drawing.Point(148, 94);
            this.txtParametros.Name = "txtParametros";
            this.txtParametros.Size = new System.Drawing.Size(100, 22);
            this.txtParametros.TabIndex = 3;
            // 
            // btnXml
            // 
            this.btnXml.Location = new System.Drawing.Point(326, 12);
            this.btnXml.Name = "btnXml";
            this.btnXml.Size = new System.Drawing.Size(112, 44);
            this.btnXml.TabIndex = 4;
            this.btnXml.Text = "Crear XML";
            this.btnXml.UseVisualStyleBackColor = true;
            this.btnXml.Click += new System.EventHandler(this.btnXml_Click);
            // 
            // dtProcesos
            // 
            this.dtProcesos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtProcesos.Location = new System.Drawing.Point(475, 12);
            this.dtProcesos.Name = "dtProcesos";
            this.dtProcesos.RowTemplate.Height = 24;
            this.dtProcesos.Size = new System.Drawing.Size(364, 429);
            this.dtProcesos.TabIndex = 5;
            // 
            // btlListarXml
            // 
            this.btlListarXml.Location = new System.Drawing.Point(148, 191);
            this.btlListarXml.Name = "btlListarXml";
            this.btlListarXml.Size = new System.Drawing.Size(112, 44);
            this.btlListarXml.TabIndex = 6;
            this.btlListarXml.Text = "Listar XML";
            this.btlListarXml.UseVisualStyleBackColor = true;
            this.btlListarXml.Click += new System.EventHandler(this.btlListarXml_Click);
            // 
            // btnListarSql
            // 
            this.btnListarSql.Location = new System.Drawing.Point(315, 191);
            this.btnListarSql.Name = "btnListarSql";
            this.btnListarSql.Size = new System.Drawing.Size(112, 44);
            this.btnListarSql.TabIndex = 7;
            this.btnListarSql.Text = "Listar SQL";
            this.btnListarSql.UseVisualStyleBackColor = true;
            this.btnListarSql.Click += new System.EventHandler(this.btnListarSql_Click);
            // 
            // btnGuardarSql
            // 
            this.btnGuardarSql.Location = new System.Drawing.Point(148, 257);
            this.btnGuardarSql.Name = "btnGuardarSql";
            this.btnGuardarSql.Size = new System.Drawing.Size(112, 44);
            this.btnGuardarSql.TabIndex = 8;
            this.btnGuardarSql.Text = "Guardar XML";
            this.btnGuardarSql.UseVisualStyleBackColor = true;
            this.btnGuardarSql.Click += new System.EventHandler(this.btnGuardarSql_Click);
            // 
            // btnGuardaSql
            // 
            this.btnGuardaSql.Location = new System.Drawing.Point(315, 260);
            this.btnGuardaSql.Name = "btnGuardaSql";
            this.btnGuardaSql.Size = new System.Drawing.Size(112, 44);
            this.btnGuardaSql.TabIndex = 9;
            this.btnGuardaSql.Text = "Guardar SQL";
            this.btnGuardaSql.UseVisualStyleBackColor = true;
            this.btnGuardaSql.Click += new System.EventHandler(this.btnGuardaSql_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 453);
            this.Controls.Add(this.btnGuardaSql);
            this.Controls.Add(this.btnGuardarSql);
            this.Controls.Add(this.btnListarSql);
            this.Controls.Add(this.btlListarXml);
            this.Controls.Add(this.dtProcesos);
            this.Controls.Add(this.btnXml);
            this.Controls.Add(this.txtParametros);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.lstProcesos);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtProcesos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstProcesos;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtParametros;
        private System.Windows.Forms.Button btnXml;
        private System.Windows.Forms.DataGridView dtProcesos;
        private System.Windows.Forms.Button btlListarXml;
        private System.Windows.Forms.Button btnListarSql;
        private System.Windows.Forms.Button btnGuardarSql;
        private System.Windows.Forms.Button btnGuardaSql;
    }
}

